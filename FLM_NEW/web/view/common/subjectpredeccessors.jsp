<%-- 
    Document   : accountList
    Created on : May 23, 2023, 2:18:24 AM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" >
        <title>Subject Predecessors</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Premium Bootstrap 4 Landing Page Template" >
        <meta name="keywords" content="Appointment, Booking, System, Dashboard, Health" >
        <meta name="author" content="Shreethemes" >
        <meta name="email" content="support@shreethemes.in" >
        <meta name="website" content="../../../../../../index.html" >
        <meta name="Version" content="v1.2.0" >

        <!-- favicon -->
        <link rel="shortcut icon" href="../../assets/images/flm-dark.png">
        <!-- Bootstrap -->
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" >
        <!-- simplebar -->
        <link href="../../assets/css/simplebar.css" rel="stylesheet" type="text/css" >
        <!-- Select2 -->
        <link href="../../assets/css/select2.min.css" rel="stylesheet" >
        <!-- Date picker -->
        <link rel="stylesheet" href="../../assets/css/flatpickr.min.css">
        <link href="../../assets/css/jquery.timepicker.min.css" rel="stylesheet" type="text/css" >
        <!-- Icons -->
        <link href="../../assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" >
        <link href="../../assets/css/remixicon.css" rel="stylesheet" type="text/css" >
        <link href="https://unicons.iconscout.com/release/v3.0.6/css/line.css"  rel="stylesheet">
        <!-- Css -->
        <link href="../../assets/css/style.min.css" rel="stylesheet" type="text/css" id="theme-opt" >
        <!--account css-->
        <link rel="stylesheet" href="../../Style/Account.css">
        <style>
            .cell-breakWord {
                word-wrap: break-word;
                max-width: 1px;
                -webkit-hyphens: auto; /* iOS 4.2+ */
                -moz-hyphens: auto; /* Firefox 5+ */
                -ms-hyphens: auto; /* IE 10+ */
                hyphens: auto;
            }
            .table1, th, td {
                border: 1px solid #dee2e6;
                vertical-align: top;
                text-align: left;
                padding: 8px;
                border-collapse: collapse;
            }

            .border-bottom p-3 {
                /*            border: 1px solid #dee2e6;*/
                vertical-align: top;
                text-align: left;
            }
            .p-3 {
                /*            border: px solid rgba(0,0,0,.05);*/
                vertical-align: top;
                text-align: left;
            }
            .mb-0 tr:nth-child(odd) {
                background-color: #f2f2f2;
            }
            .mb-0 tr:hover {
                background-color: #ddd;
                ;
            }
        </style>
    </head>

    <body>
        <!-- Loader -->
        <div id="preloader">
            <div id="status">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
        </div>
        <!-- Loader -->

        <div class="page-wrapper doctris-theme toggled">
            <%@include file="../common/sidebar.jsp" %>
            <!-- sidebar-wrapper  -->

            <!-- Start Page Content -->
            <main class="page-content bg-light">
                <%@include file="../common/headerforsidebar.jsp" %>
                <div class="container-fluid">
                    <div class="layout-specing">
                        <div class="d-md-flex justify-content-between">

                            <nav aria-label="breadcrumb" class="d-inline-block mt-4 mt-sm-0">
                                <ul class="breadcrumb bg-transparent rounded mb-0 p-0">
                                    <li class="breadcrumb-item"><a href="/FLM_NEW/view/common/home">ADMIN</a></li>
                                    <li class="breadcrumb-item active"><a href="/FLM_NEW/view/common/subjectpredeccessors">Subject Predeccessors</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div >
                            <div class="row" >
                                <div class="col-12" style="max-width:95%;margin: auto">
                                    <h2 style="margin-top:10px">Show Learning Path of a Subject</h2>
                                    <form action="subjectpredeccessors" method="GET">
                                        <div class="mb-2" style="max-width:95%;margin: auto; margin-top:20px">
                                            <input type="text" id="code" name="code" class="border bg-white rounded-pill"
                                                   placeholder="Subject Code:" 
                                                   style="height: 40px; width: 250px; border: 1px solid #dee2e6;">
                                            <button type="submit" name="search" class="btn btn-pills btn-primary"><i class="ri-search-line align-middle me-1"></i> Search</button>
                                        </div>
                                        <div style="max-width:95%;margin: auto;"><h5 style="color: red">${errorSearch}</h5></div>
                                        <div class="mb-2" >
                                            <div style="font-style: initial; margin-top:20px">All ? syllabus(es)</div>
                                        </div>                                        
                                    </form>
                                </div>
                                <!-- Table -->
                                <div class="col-12" style="max-width:95%; margin: auto">
                                    <div class="table-responsive bg-white shadow rounded">
                                        <table class="table table-center table-padding mb-0" >
                                            <col style="width:5%">
                                            <col style="width:10%">
                                            <col style="width:20%">
                                            <col style="width:20%">
                                            <col style="width:20%">
                                            <col style="width:35%">
                                            <thead>
                                                <tr>
                                                    <th class="border-bottom p-3" style="background-color:#396cf0; color: #ffffff">ID</th>
                                                    <th class="border-bottom p-3" style="background-color:#396cf0; color: #ffffff">Syllabus ID</th>
                                                    <th class="border-bottom p-3" style="background-color:#396cf0; color: #ffffff">Subject Name</th>
                                                    <th class="border-bottom p-3" style="background-color:#396cf0; color: #ffffff">Syllabus Name</th>
                                                    <th class="border-bottom p-3" style="background-color:#396cf0; color: #ffffff">DecisionNo MM/dd/yyyy</th>
                                                    <th class="border-bottom p-3" style="background-color:#396cf0; color: #ffffff">All subjects need to learn before</th>
                                                </tr>
                                            </thead>
                                            <tbody class="listAccount">
                                                <tr class="itemAccount">
                                                    <td class="p-3 " >1</td>
                                                    <td class="p-3 " >1234567891234</td>
                                                    <td class="p-3 cell-breakWord" >asdvasdvasdvasdvaasdvasdvasdvasdvaasdvasdvasdvasdvaasdvasdvasdvasdvaasdvasdvasdvasdva</td>
                                                    <td class="p-3 " >vasdvasdvasdvasd</td>
                                                    <td class="p-3 " >asdvasdvasdvasd</td>
                                                    <td class="p-3 cell-breakWord" >asdvasdvasdvasdvaasdvasdvasdvasdvaasdvasdvasdvasdvaasdvasdvasdvasdvaasdvasdvasdvasdva</td>
                                                </tr>
                                            </tbody>
                                        </table>                                                                      
                                        <!-- END Table -->                                      
                                    </div>
                                </div>
                            </div>
                        </div>                   

                        <div class="row text-center">
                            <!-- PAGINATION START -->
                            <div class="col-12" style="max-width:95%;margin: auto; margin-top:20px">
                                <div class="d-md-flex align-items-center text-center justify-content-between">
                                    <span id="numberAcc" class="text-muted me-3"></span>
                                    <ul class="listPage pagination justify-content-center mb-0 mt-3 mt-sm-0">
                                    </ul>
                                </div>
                            </div><!--end col-->
                            <!-- PAGINATION END -->
                        </div><!--end row-->
                    </div>
                </div><!--end container-->

                <!-- Footer Start -->
                <%@include file="../common/footerforsidebar.jsp" %>
                <!--end footer-->
                <!-- End -->
            </main>
            <!--End page-content" -->
        </div>
        <!-- page-wrapper -->


        <!-- javascript -->
        <script src="../../assets/js/jquery.min.js"></script>
        <script src="../../assets/js/bootstrap.bundle.min.js"></script>
        <!-- simplebar -->
        <script src="../../assets/js/simplebar.min.js"></script>
        <!-- Select2 -->
        <script src="../../assets/js/select2.min.js"></script>
        <script src="../../assets/js/select2.init.js"></script>
        <!-- Datepicker -->
        <script src="../../assets/js/flatpickr.min.js"></script>
        <script src="../../assets/js/flatpickr.init.js"></script>
        <!-- Datepicker -->
        <script src="../../assets/js/jquery.timepicker.min.js"></script> 
        <script src="../../assets/js/timepicker.init.js"></script> 
        <!-- Icons -->
        <script src="../../assets/js/feather.min.js"></script>
        <!-- Main Js -->
        <script src="../../assets/js/app.js"></script>
        <!--account list-->
        <script src="../../JS/AccountList.js"></script>

    </body>

</html>
