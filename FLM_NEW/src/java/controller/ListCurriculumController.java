/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.CurriculumDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import model.Curriculum;

/**
 *
 * @author hp
 */
public class ListCurriculumController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            request.getRequestDispatcher("/view/curriculum/listcurriculum.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.setAttribute("role", "guest");
        request.setAttribute("op", 1);
//        request.setAttribute("op", "3");
//        processRequest(request, response);
        request.getRequestDispatcher("listcurriculum.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String optionSearch = request.getParameter("optionSearch");

        try {

            //Search by code
            if (request.getParameter("btn-search") != null && optionSearch.equals("1")) {

                String code = request.getParameter("code").trim() == null ? "" : request.getParameter("code").trim();
                //check code contrain 
                if (code.length() > 20) {
                    throw new Exception();
                } else {
                    //List curriculum
                    CurriculumDAO c = new CurriculumDAO();
                    ArrayList<Curriculum> List = c.searchListCurriculum(code);
                    String count = c.NumberOfRow(code);
                    
                    request.setAttribute("select", "1");
                    request.setAttribute("count", count);
                    request.setAttribute("op", 1);
                    request.setAttribute("code", code);
                    request.setAttribute("List", List);
                    request.getRequestDispatcher("listcurriculum.jsp").forward(request, response);
                }

            } else {
                if (request.getParameter("btn-search") != null && optionSearch.equals("2")) {

                    String name = request.getParameter("code").trim() == null ? "" : request.getParameter("code").trim();
                    //check code contrain 
                    if (name.length() > 20) {
                        throw new Exception();
                    } else {
                        //List curriculum
                        CurriculumDAO c = new CurriculumDAO();
                        ArrayList<Curriculum> List = c.searchListCurriculumByName(name);
                        String count = c.NumberOfRow(name);
                        
                        
                        request.setAttribute("select", "2");
                        request.setAttribute("count", count);
                        request.setAttribute("op", 1);
                        request.setAttribute("code", name);
                        request.setAttribute("List", List);
                        request.getRequestDispatcher("listcurriculum.jsp").forward(request, response);
                    }
                }
            }

        } catch (Exception e) {
            String code = request.getParameter("code").trim() == null ? "" : request.getParameter("code").trim();
            request.setAttribute("code", code);
            request.setAttribute("err", "Code must be less than 20 characters");
            request.getRequestDispatcher("listcurriculum.jsp").forward(request, response);
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
