/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import dao.UserDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 *
 * @author Admin
 */
public class RsPwByMobileController extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserDAO ud = new UserDAO();
        String mobile = req.getParameter("mobile");
        String password = req.getParameter("password");
        ud.updatePasswordByMobile(password, mobile);
        req.setAttribute("ChangeSuccess", "1");
        req.getRequestDispatcher("forgotpasswordbymobile.jsp").forward(req, resp);
        
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("ChangeSuccess", "0");
        req.setAttribute("sendCode", "0");
        req.getRequestDispatcher("forgotpasswordbymobile.jsp").forward(req, resp);
    }

}
