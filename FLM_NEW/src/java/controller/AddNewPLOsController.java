/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import dao.CurriculumDAO;
import dao.PloDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import model.Curriculum;

/**
 *
 * @author Admin
 */
public class AddNewPLOsController extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String ploID = req.getParameter("ploID");
        String ploName = req.getParameter("ploName");
        String ploDescription = req.getParameter("ploDescription");
        String Ccode = req.getParameter("Ccode");
      
//        System.out.println(ploID);
//        System.out.println(ploName);
//        System.out.println(ploDescription);
//        System.out.println(Ccode);
//        String alert = "";
        PloDAO pDAO = new PloDAO();
        CurriculumDAO cDAO = new CurriculumDAO();
        Curriculum c = cDAO.getCurriculumOverview(Ccode);
        
        if (req.getParameter("btn") != null && pDAO.checkIdPlo(ploID)==false) {
//            System.out.println("bam vao day r ");
            pDAO.addPloByCcode(ploID, ploName, ploDescription, Ccode,"1");
            req.setAttribute("alert", "Add PLO Successfull !");
            req.setAttribute("op", "4");
            req.setAttribute("Ccode", Ccode);
            req.setAttribute("c", c);
            req.getRequestDispatcher("addplos.jsp").forward(req, resp);
        }
        else{
            req.setAttribute("alert", "PLO ID is exist !");
            req.setAttribute("op", "4");
            req.setAttribute("Ccode", Ccode);
            req.setAttribute("c", c);
            req.getRequestDispatcher("addplos.jsp").forward(req, resp);
        }

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String Ccode = req.getParameter("Ccode");
        CurriculumDAO cDAO = new CurriculumDAO();
        Curriculum c = cDAO.getCurriculumOverview(Ccode);
        req.setAttribute("c", c);
        req.setAttribute("op", "4");
        req.getRequestDispatcher("addplos.jsp").forward(req, resp);
    }

}
