/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import dao.SettingDAO;
import dao.SubjectDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import model.Setting;
import model.Subject;

/**
 *
 * @author Admin
 */
public class SubjectController extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String action = req.getParameter("action");
            String search = req.getParameter("search");
            SubjectDAO sd = new SubjectDAO();
            String btn = req.getParameter("btn");
            if (btn != null) {
                String name = req.getParameter("name");
                String code = req.getParameter("code");
                String parentid = req.getParameter("parentid");
                String isActive = req.getParameter("isActive");
                isActive = isActive == null ? "0" : "1";
                String description = req.getParameter("description");
                System.out.println(name);
                System.out.println(code);
                System.out.println(parentid);
                System.out.println(isActive);
                System.out.println(description);
                if (sd.checkDuplicateCodeSubject(code) != null) {
                    req.setAttribute("nameNew", name);
                    req.setAttribute("codeNew", code);
//                    req.setAttribute("isActiveNew", isActive);
                    req.setAttribute("descriptionNew", description);
                    ArrayList<Subject> listParentCode = sd.getListCodeIdSubject();
                    req.setAttribute("listParentCode", listParentCode);
                    req.setAttribute("alertError", "Subject CODE is available. Please enter another CODE !");
                    req.getRequestDispatcher("addsubject.jsp").forward(req, resp);
                } else if (sd.checkDuplicateNameSubject(name) != null) {
                    req.setAttribute("nameNew", name);
                    req.setAttribute("codeNew", code);
//                    req.setAttribute("isActiveNew", isActive);
                    req.setAttribute("descriptionNew", description);
                    ArrayList<Subject> listParentCode = sd.getListCodeIdSubject();
                    req.setAttribute("listParentCode", listParentCode);
                    req.setAttribute("alertError", "Subject NAME is available. Please enter another NAME !");
                    req.getRequestDispatcher("addsubject.jsp").forward(req, resp);
                } else {
                    if (parentid == null || "".equals(parentid)) {
                        int newId = Integer.parseInt(sd.getSubjectIdMax().getId()) + 1;
                        sd.addSubjectNoneParent(String.valueOf(newId), name, code, isActive, description);
                        req.setAttribute("nameNew", name);
                        req.setAttribute("codeNew", code);
                        req.setAttribute("descriptionNew", description);
                        ArrayList<Subject> listParentCode = sd.getListCodeIdSubject();
                        req.setAttribute("listParentCode", listParentCode);
                        req.setAttribute("alert", "Successfully added new subject !");
                        req.getRequestDispatcher("addsubject.jsp").forward(req, resp);
                    } else {
                        int newId = Integer.parseInt(sd.getSubjectIdMax().getId()) + 1;
                        sd.addSubject(String.valueOf(newId), name, code, parentid, isActive, description);
                        req.setAttribute("nameNew", name);
                        req.setAttribute("codeNew", code);
                        req.setAttribute("descriptionNew", description);
                        ArrayList<Subject> listParentCode = sd.getListCodeIdSubject();
                        req.setAttribute("parentCodeNew", sd.getParentCodeByParentId(parentid).getCode());
                        req.setAttribute("parentIdNew", parentid);
                        req.setAttribute("listParentCode", listParentCode);
                        req.setAttribute("alert", "Successfully added new subject !");
                        req.getRequestDispatcher("addsubject.jsp").forward(req, resp);
                    }
                }
            }
            if (action.equals("edit")) {
                String id = req.getParameter("idValue");
                String name = req.getParameter("name");
                String code = req.getParameter("code");
                String type = req.getParameter("selectType");
                String isActive = req.getParameter("isActive");
                isActive = isActive == null ? "0" : "1";
                String description = req.getParameter("description");
                String parentSubjectId = req.getParameter("selectSubject");
                SubjectDAO sD = new SubjectDAO();
                sD.updateSubjectById(id, name, code, type,isActive, parentSubjectId, description);
            }
            if (action.equals("delete")) {

                String id = req.getParameter("deleteSubjectById");
                SubjectDAO sD = new SubjectDAO();
//            sD.deleteSubjectById(id);
            }
            SettingDAO stD = new SettingDAO();
            ArrayList<Setting> listType = stD.getListSubjectType();
            req.setAttribute("listType", listType);
            req.setAttribute("search", search);
            SubjectDAO sD = new SubjectDAO();
            ArrayList<Subject> listSubject = sD.getListOfSubject(search);
            req.setAttribute("listSubject", listSubject);
            req.setAttribute("alert", "");
            req.getRequestDispatcher("subjectList.jsp").forward(req, resp);
        } catch (ServletException | IOException e) {
            System.out.println("error doPost SubjectController: " + e.getMessage());
        }

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String action = req.getParameter("action");
            String search = req.getParameter("search");
            SubjectDAO sd = new SubjectDAO();
            if (action.equals("list")) {
                //first run
                if (search == null) {
                    search = "";
                } else {
                    req.setAttribute("search", search);
                }
                SubjectDAO sD = new SubjectDAO();
                ArrayList<Subject> listSubject = sD.getListOfSubject(search);
                SettingDAO stD = new SettingDAO();
                ArrayList<Setting> listType = stD.getListSubjectType();
                req.setAttribute("listSubject", listSubject);
                req.setAttribute("listType", listType);
                req.setAttribute("alert", "");
                req.getRequestDispatcher("subjectList.jsp").forward(req, resp);
            }
            if (action.equals("add")) {
                ArrayList<Subject> listParentCode = sd.getListCodeIdSubject();
                req.setAttribute("listParentCode", listParentCode);
                req.getRequestDispatcher("addsubject.jsp").forward(req, resp);
            }

        } catch (ServletException | IOException e) {
            System.out.println("error doGet SubjectController: " + e.getMessage());
        }

    }

}
