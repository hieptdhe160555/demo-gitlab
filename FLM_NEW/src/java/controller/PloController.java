/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package controller;

import dao.CurriculumDAO;
import dao.PloDAO;
import dao.UserDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import model.Curriculum;
import model.Plo;

/**
 *
 * @author Admin
 */
public class PloController extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String Ccode = req.getParameter("Ccode");
        PloDAO pDao = new PloDAO();
        CurriculumDAO cDAO = new CurriculumDAO();
        Curriculum c = cDAO.getCurriculumOverview(Ccode);

        if (req.getParameter("mod") != null && req.getParameter("mod").equals("2")) {
            String idPlo = req.getParameter("IdPlo");
//            System.out.println(idPlo);
            pDao.deletePloById(idPlo);
            req.setAttribute("alert", "");
            ArrayList<Plo> ListPLO = pDao.getListPloByCode(Ccode);
            req.setAttribute("op", "4");
            req.setAttribute("c", c);
            req.setAttribute("ListPLO", ListPLO);
            req.getRequestDispatcher("plo.jsp").forward(req, resp);

        }

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String Ccode = req.getParameter("Ccode");
       
        CurriculumDAO cDAO = new CurriculumDAO();
        Curriculum c = cDAO.getCurriculumOverview(Ccode);
        PloDAO pDao = new PloDAO();
        
        ArrayList<Plo> ListPLO = pDao.getListPloByCode(Ccode);
        req.setAttribute("op", "4");
        req.setAttribute("c", c);
        req.setAttribute("ListPLO", ListPLO);
        req.getRequestDispatcher("plo.jsp").forward(req, resp);
    }

}
