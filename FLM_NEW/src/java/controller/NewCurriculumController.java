/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import dao.CurriculumDAO;
import dao.DecisionDAO;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import model.Curriculum;
import model.Decision;


/**
 *
 * @author hp
 */
public class NewCurriculumController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet NewCurriculumController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet NewCurriculumController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("newcurriculum.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                String id = request.getParameter("id");
        
   //                String id = request.getParameter("id");

        String code = request.getParameter("code");
        String name = request.getParameter("name");
        String decision_ID = request.getParameter("decisionID");
        String total_credit = request.getParameter("total_credit");
        String Owner_id = request.getParameter("Owner_id");
//        String is_active = request.getParameter("is_active");
        boolean is_active = false;
        String description = request.getParameter("description");
        System.out.println(Owner_id);
        try {
            if (code.trim().length() <= 2 || code.trim().length() > 20) {
                request.setAttribute("errCode", "Error curriculum code !");
                throw new Error();
            }
//            int Cid = Integer.parseInt(id);
            int Did = Integer.parseInt(decision_ID);
            int credit = Integer.parseInt(total_credit);
//            int Oid = Integer.parseInt(Owner_id);

            //Check curriculum
            CurriculumDAO cDAO = new CurriculumDAO();
            Curriculum c = cDAO.CheckCurriculumByID(code);
            int id_next = cDAO.getMaxCurriculumId() + 1;
            String c_id = String.valueOf(id_next);
            //Check Decision
            DecisionDAO dDAO = new DecisionDAO();
            Decision d = dDAO.CheckDecisionByID(Did);

//            System.out.println(cDAO.getMaxCurriculumId());
            if (c == null) {
                if (d != null) {
                    Curriculum cnew = new Curriculum(c_id, code, name, description, decision_ID, credit, Owner_id, is_active);
                    cDAO.InsertCurriculum(cnew);

                    //infor

                    request.setAttribute("code", code);
                    request.setAttribute("name", name);
                    request.setAttribute("decision_ID", decision_ID);
                    request.setAttribute("total_credit", total_credit);
//                    request.setAttribute("Owner_id", Owner_id);
                    request.setAttribute("is_active", is_active);
                    request.setAttribute("description", description);
                    //mess
                    request.setAttribute("Done", "Add suscessfully");

                    request.getRequestDispatcher("newcurriculum.jsp").forward(request, response);
                } else {
                    //errr
                request.setAttribute("DecisionId", "Decision non-exist in database!  ");
                //infor
//                request.setAttribute("id", Cid);
                request.setAttribute("code", code);
                request.setAttribute("name", name);
                request.setAttribute("decision_ID", decision_ID);
                request.setAttribute("total_credit", total_credit);
                request.setAttribute("Owner_id", Owner_id);
                request.setAttribute("is_active", is_active);
                request.setAttribute("description", description);
                request.getRequestDispatcher("newcurriculum.jsp").forward(request, response);
                }

            } else {
                //errr
                request.setAttribute("CurriculumId", "Curriculum already exist in database!  ");
                //infor
//                request.setAttribute("id", Cid);
                request.setAttribute("code", code);
                request.setAttribute("name", name);
                request.setAttribute("decision_ID", decision_ID);
                request.setAttribute("total_credit", total_credit);
                request.setAttribute("Owner_id", Owner_id);
                request.setAttribute("is_active", is_active);
                request.setAttribute("description", description);
                request.getRequestDispatcher("newcurriculum.jsp").forward(request, response);
            }

        } catch (Exception e) {
            // re-up input
//            request.setAttribute("id", id);
            request.setAttribute("code", code);
            request.setAttribute("name", name);
            request.setAttribute("decision_ID", decision_ID);
            request.setAttribute("total_credit", total_credit);
            request.setAttribute("Owner_id", Owner_id);
            request.setAttribute("is_active", is_active);
            request.setAttribute("description", description);
            // Mess error

            request.setAttribute("err", "Decision_ID, Total_credit must be positive integer");

            request.getRequestDispatcher("newcurriculum.jsp").forward(request, response);
        } catch (Error ex) {
            // re-up input
//            request.setAttribute("id", id);
            request.setAttribute("code", code);
            request.setAttribute("name", name);
            request.setAttribute("decision_ID", decision_ID);
            request.setAttribute("total_credit", total_credit);
            request.setAttribute("Owner_id", Owner_id);
            request.setAttribute("is_active", is_active);
            request.setAttribute("description", description);

            request.getRequestDispatcher("newcurriculum.jsp").forward(request, response);
        }
        
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
