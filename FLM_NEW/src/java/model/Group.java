/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author user
 */
public class Group {

    String id;
    String name;
    String curriculum_id;
    String combo_subject;

    public Group() {
    }

    public Group(String id) {
        this.id = id;
    }

    public Group(String id, String name, String combo_subject) {
        this.id = id;
        this.name = name;
        this.combo_subject = combo_subject;
    }

    public Group(String id, String name, String curriculum_id, String combo_subject) {
        this.id = id;
        this.name = name;
        this.curriculum_id = curriculum_id;
        this.combo_subject = combo_subject;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCurriculum_id() {
        return curriculum_id;
    }

    public void setCurriculum_id(String curriculum_id) {
        this.curriculum_id = curriculum_id;
    }

    public String getCombo_subject() {
        return combo_subject;
    }

    public void setCombo_subject(String combo_subject) {
        this.combo_subject = combo_subject;
    }

    @Override
    public String toString() {
        return "Group{" + "id=" + id + ", name=" + name + ", curriculum_id=" + curriculum_id + ", combo_subject=" + combo_subject + '}';
    }

}
