/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Plo_Po;

/**
 *
 * @author Admin
 */
public class Plo_PoDAO extends BaseDAO {

    public ArrayList<Plo_Po> getMappings(String Ccode) {
        ArrayList<Plo_Po> list = new ArrayList<>();

        try {
            String query = "SELECT pp.po_id, pp.plo_id\n"
                    + "FROM flm_db.po_plo pp\n"
                    + "JOIN flm_db.po po ON pp.po_id = po.po_id\n"
                    + "JOIN flm_db.plo p ON pp.plo_id = p.plo_id\n"
                    + "WHERE p.curriculum_id = (SELECT c.curriculum_id FROM flm_db.curriculum c WHERE c.code = ?)\n"
                    + "\n"
                    + "UNION ALL\n"
                    + "\n"
                    + "SELECT po.po_id, NULL AS plo_id\n"
                    + "FROM flm_db.po po\n"
                    + "LEFT JOIN flm_db.po_plo pp ON po.po_id = pp.po_id\n"
                    + "WHERE pp.po_id IS NULL AND po.curriculum_id = (SELECT c.curriculum_id FROM flm_db.curriculum c WHERE c.code = ?)\n"
                    + "\n"
                    + "UNION ALL\n"
                    + "\n"
                    + "SELECT NULL AS po_id, p.plo_id\n"
                    + "FROM flm_db.plo p\n"
                    + "LEFT JOIN flm_db.po_plo pp ON p.plo_id = pp.plo_id\n"
                    + "WHERE pp.plo_id IS NULL AND p.curriculum_id = (SELECT c.curriculum_id FROM flm_db.curriculum c WHERE c.code = ?);";
            PreparedStatement pstm = connection.prepareStatement(query);
            pstm.setString(1, Ccode);
            pstm.setString(2, Ccode);
            pstm.setString(3, Ccode);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                String poId = rs.getString("po_id");
                String ploId = rs.getString("plo_id");
                list.add(new Plo_Po(poId, ploId));
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
        return list;
    }

    public boolean check(String plo, String po) {
        try {
            String sql
                    = "select pp.po_id FROM flm_db.po_plo pp\n"
                    + "where pp.po_id = ? and pp.plo_id = ?;";
            PreparedStatement pstm = connection.prepareStatement(sql);
            pstm.setString(1, po);
            pstm.setString(2, plo);
            ResultSet rs = pstm.executeQuery();
            if (rs.next()) {
                return true;
            }
        } catch (Exception ex) {
            System.out.println("Loi :" + ex.getMessage());
        }
        return false;
    }

    public void delete(String plo, String po) {
        try {
            String sqlDetele = ""
                    + "DELETE FROM `po_plo`\n"
                    + "WHERE po_id = ? AND plo_id = ?";
            PreparedStatement pstm = connection.prepareStatement(sqlDetele);

            pstm.setString(1, po);
            pstm.setString(2, plo);
            pstm.execute();

        } catch (Exception ex) {
            System.out.println("delete :" + ex.getMessage());

        }
    }

    public void addMappingPLO_PO(String plo, String po) {

        try {
            String sqlUpdate = ""
                    + "INSERT INTO `flm_db`.`po_plo` (`po_id`, `plo_id`) VALUES (?, ?);";
            PreparedStatement pstm = connection.prepareStatement(sqlUpdate);
            pstm.setString(1, po);
            pstm.setString(2, plo);
            pstm.executeUpdate();

        } catch (Exception ex) {
            System.out.println("addMappingPLO_PO :" + ex.getMessage());
        }

    }

    public static void main(String[] args) {
       
        Plo_PoDAO p = new Plo_PoDAO();
        p.delete("52", "2");
          ArrayList<Plo_Po> list = p.getMappings("BBA_FIN_K16D17A");
          for (Plo_Po plo_Po : list) {
              System.out.println(plo_Po);
        }
        
    }
}
