/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import model.Curriculum;

/**
 *
 * @author hp
 */
public class CurriculumDAO extends BaseDAO {

    public ArrayList<Curriculum> searchListCurriculum(String code) {
        ArrayList<Curriculum> data = new ArrayList<>();
        try {

            //Step 1:           
            String sql = "select * from curriculum c join decision d \n"
                    + "on c.decision_id = d.decision_id \n"
                    + "where code like ? ";
            //step 2:        
            PreparedStatement pstm = connection.prepareStatement(sql);
            pstm.setString(1, "%" + code + "%");
//            pstm.setString(1, code);
//            System.out.println("run5");
            ResultSet rs = pstm.executeQuery();
            //step 3:        
            while (rs.next()) {
                Curriculum c = new Curriculum(rs.getString(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getInt(6), rs.getString(7), rs.getBoolean(8), rs.getString(10), rs.getString(11));
                data.add(c);
            }

        } catch (Exception e) {
            System.out.println("searchListCurriculum()" + e.getMessage());
        }
        return data;
    }

    public Curriculum getCurriculumOverview(String Ccode) {
        Curriculum c = new Curriculum();
        try {
            String sql = "select * from curriculum c join decision d \n"
                    + "on c.decision_id = d.decision_id \n"
                    + "where code like ? ";
            PreparedStatement pstm = connection.prepareStatement(sql);
            pstm.setString(1, Ccode);
            ResultSet rs = pstm.executeQuery();

            while (rs.next()) {
                c.setCurriculum_id(rs.getString(1));
                c.setCode(rs.getString(2));
                c.setName(rs.getString(3));
                c.setDescription(rs.getString(4));
                c.setDecision_id(rs.getString(5));
                c.setTotal_credit(rs.getInt(6));
                c.setOwner_id(rs.getString(7));
                c.setIs_active(rs.getBoolean(8));
                c.setDecision_no(rs.getString(10));
                c.setDecision_date(rs.getString(11));
            }

        } catch (Exception e) {
            System.out.println("getCurriculumOverview" + e.getMessage());
        }
        return c;
    }

    public String NumberOfRow(String code) {

        try {
            String sql = "select count(*)\n"
                    + "from curriculum c join decision d \n"
                    + "on c.decision_id = d.decision_id \n"
                    + "where code like ? ";
            PreparedStatement pstm = connection.prepareStatement(sql);
            pstm.setString(1, "%" + code + "%");
            ResultSet rs = pstm.executeQuery();

            while (rs.next()) {
                String count = rs.getString(1);
                System.out.println(count);
                return count;

            }

        } catch (Exception e) {
            System.out.println("getCurriculumOverview" + e.getMessage());

        }
        return null;
    }

    public Curriculum getCurriculumById(String id) {
        Curriculum c = new Curriculum();
        try {
            String sql = "select * from curriculum c join decision d "
                    + "on c.decision_id = d.decision_id \n"
                    + "where curriculum_id =?;";
            PreparedStatement pstm = connection.prepareStatement(sql);
            pstm.setString(1, id);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                c.setCurriculum_id(rs.getString(1));
                c.setCode(rs.getString(2));
                c.setName(rs.getString(3));
                c.setDescription(rs.getString(4));
                c.setDecision_id(rs.getString(5));
                c.setTotal_credit(rs.getInt(6));
                c.setOwner_id(rs.getString(7));
                c.setIs_active(rs.getBoolean(8));
                c.setDecision_no(rs.getString(10));
                c.setDecision_date(rs.getString(11));
            }
        } catch (SQLException e) {
            System.out.println("error getCurriculumById" + e.getMessage());
        }
        return c;
    }

    public void InsertCurriculum(Curriculum c) {

        try {
            String strInsert = "INSERT INTO `flm_db`.`curriculum` (`curriculum_id`,`code`, `name`, `description`, `decision_id`, `total_credit`, `owner_id`, `is_active`)"
                    + " VALUES ( ?,?, ?, ?, ?, ?, ?, ?);";
            PreparedStatement pstm = connection.prepareStatement(strInsert);
            pstm.setString(1, c.getCurriculum_id());
            pstm.setString(2, c.getCode());
            pstm.setString(3, c.getName());
            pstm.setString(4, c.getDescription());
            pstm.setString(5, c.getDecision_id());
            pstm.setInt(6, c.getTotal_credit());
            pstm.setString(7, c.getOwner_id());
            pstm.setBoolean(8, c.getIs_active());
            pstm.executeUpdate();

        } catch (Exception e) {
            System.out.println("InsertCurriculum " + e.getMessage());
        }
    }

    public Curriculum CheckCurriculumByID(int id) {
        try {
            String sql = "select * from curriculum where curriculum_id = ?  ";
            PreparedStatement pstm = connection.prepareStatement(sql);
            pstm.setInt(1, id);
            ResultSet rs = pstm.executeQuery();
            if (rs.next()) {
                Curriculum c = new Curriculum(rs.getString(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getInt(6),
                        rs.getString(7), rs.getBoolean(8));
                System.out.println("run");
                return c;

            }

        } catch (SQLException e) {
            System.out.println("error CheckCurriculumByID" + e.getMessage());
        }
        return null;
    }

    public void updateCurriculum(Curriculum c) {
        try {
            String strInsert = "UPDATE `flm_db`.`curriculum` SET `code` = ?, `name` = ?, `description` = ?, `decision_id` = ? ,\n"
                    + " `total_credit` = ?, `owner_id` = ? ,  `is_active` = ? \n"
                    + "WHERE (`curriculum_id` = ?)";
            PreparedStatement pstm = connection.prepareStatement(strInsert);

            pstm.setString(1, c.getCode());
            pstm.setString(2, c.getName());
            pstm.setString(3, c.getDescription());
            pstm.setString(4, c.getDecision_id());
            pstm.setInt(5, c.getTotal_credit());
            pstm.setString(6, c.getOwner_id());
            pstm.setBoolean(7, c.getIs_active());
            pstm.setString(8, c.getCurriculum_id());
            pstm.executeUpdate();

        } catch (Exception e) {
            System.out.println("updateCurriculum " + e.getMessage());
        }
    }

    public ArrayList<Curriculum> getListCurriculum() {
        ArrayList<Curriculum> listCurriculum = new ArrayList<>();
        try {
            String strSelect = "SELECT * FROM flm_db.curriculum;";
            PreparedStatement pstm = connection.prepareStatement(strSelect);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                listCurriculum.add(new Curriculum(rs.getString(1), rs.getString(2),
                        rs.getString(3), rs.getString(4), rs.getString(5), rs.getInt(6),
                        rs.getString(7), rs.getBoolean(8)));
            }
            return listCurriculum;
        } catch (SQLException e) {
            System.out.println("error getListCurriculum " + e.getMessage());
        }
        return null;
    }

    public Curriculum getCurriculumCodeByCurriculumId(String curriculumidcombo) {
        try {
            String strSelect = "SELECT curriculum_id, code FROM flm_db.curriculum "
                    + "WHERE curriculum_id=?;";
            PreparedStatement pstm = connection.prepareStatement(strSelect);
            pstm.setString(1, curriculumidcombo);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                return new Curriculum(rs.getString(1), rs.getString(2));
            }
        } catch (SQLException e) {
            System.out.println("error getCurriculumCodeByCurriculumId: " + e.getMessage());
        }
        return null;
    }

    public Curriculum CheckCurriculumByID(String code) {
        try {
            String sql = "select * from curriculum where code = ?  ";
            PreparedStatement pstm = connection.prepareStatement(sql);
            pstm.setString(1, code);
            ResultSet rs = pstm.executeQuery();
            if (rs.next()) {
                Curriculum c = new Curriculum(rs.getString(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getInt(6),
                        rs.getString(7), rs.getBoolean(8));
                System.out.println("run");
                return c;

            }

        } catch (SQLException e) {
            System.out.println("error CheckCurriculumByID" + e.getMessage());
        }
        return null;
    }

    public int getMaxCurriculumId() {
        try {
            String sql = "SELECT MAX(curriculum_id) from curriculum;";
            PreparedStatement pstm = connection.prepareStatement(sql);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                int MaxId = rs.getInt(1);
                return MaxId;
            }

        } catch (Exception e) {
            System.out.println("getMaxCurriculumId()" + e.getMessage());
        }
        return 0;
    }

    public ArrayList<Curriculum> getListCurriculumWhereNot(String curriculumid) {
        ArrayList<Curriculum> listCurriculum = new ArrayList<>();
        try {
            String strSelect = "SELECT * FROM flm_db.curriculum\n"
                    + "WHERE NOT curriculum_id = ?;";
            PreparedStatement pstm = connection.prepareStatement(strSelect);
            pstm.setString(1, curriculumid);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                listCurriculum.add(new Curriculum(rs.getString(1), rs.getString(2),
                        rs.getString(3), rs.getString(4), rs.getString(5), rs.getInt(6),
                        rs.getString(7), rs.getBoolean(8)));
            }
            return listCurriculum;
        } catch (SQLException e) {
            System.out.println("error getListCurriculum " + e.getMessage());
        }
        return null;
    }

    public static void main(String[] args) {
        CurriculumDAO cd = new CurriculumDAO();
        ArrayList<Curriculum> listCurriculum = cd.getListCurriculumWhereNot("1");
        for (Curriculum curriculum : listCurriculum) {
            System.out.println(curriculum);
        }
    }

    public ArrayList<Curriculum> searchListCurriculumByName(String name) {
        
        ArrayList<Curriculum> data = new ArrayList<>();
        try {

            //Step 1:           
            String sql = "select * from curriculum c join decision d \n"
                    + "on c.decision_id = d.decision_id \n"
                    + "where name like ? ";
            //step 2:        
            PreparedStatement pstm = connection.prepareStatement(sql);
            pstm.setString(1, "%" + name + "%");
//            pstm.setString(1, code);
//            System.out.println("run5");
            ResultSet rs = pstm.executeQuery();
            //step 3:        
            while (rs.next()) {
                Curriculum c = new Curriculum(rs.getString(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getInt(6), rs.getString(7), rs.getBoolean(8), rs.getString(10), rs.getString(11));
                data.add(c);
            }

        } catch (Exception e) {
            System.out.println("searchListCurriculum()" + e.getMessage());
        }
        return data;
        
        
    }
    
    
    
    
    
    
    
}
