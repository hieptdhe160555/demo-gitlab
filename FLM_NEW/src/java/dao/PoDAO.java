/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Po;

/**
 *
 * @author Admin
 */
public class PoDAO extends BaseDAO {

    public ArrayList<Po> getListPoByCode(String Code) {
        ArrayList<Po> list = new ArrayList<>();
        try {
            String strSelect
                    = "select p.Po_id,p.Po_name,p.Po_description from Po as p join flm_db.curriculum as c\n"
                    + "on p.curriculum_id = c.curriculum_id\n"
                    + "where c.code = ?";
            PreparedStatement pstm = connection.prepareStatement(strSelect);
            pstm.setString(1, Code);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                String Po_id = rs.getString(1);
                String Po_name = rs.getString(2);
                String Po_description = rs.getString(3);
                list.add(new Po(Po_id, Po_name, Po_description));
            }
            return list;
        } catch (SQLException e) {
            System.out.println("getListPoByCode: " + e.getMessage());
        }
        return null;
    }

    public Po getPobyPoID(String Po_id) {
        try {
            String strSelect = "select p.Po_id,p.Po_name,p.Po_description,c.code,c.name from Po as p join curriculum as c\n"
                    + "on p.curriculum_id = c.curriculum_id\n"
                    + "where p.Po_id = ? ";
            PreparedStatement pstm = connection.prepareStatement(strSelect);
            pstm.setString(1, Po_id);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                return new Po(rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5));
            }
        } catch (Exception e) {
            System.out.println("getPobyNamePoAndCodeCurriculum " + e.getMessage());
        }
        return null;
    }

    public void updatePos(Po p1) {
        try {
            String StrUpdate = "update flm_db.Po \n"
                    + "	SET flm_db.Po.Po_description =?, flm_db.Po.Po_name =?\n"
                    + "	where flm_db.Po.Po_id=?";
            PreparedStatement pstm = connection.prepareStatement(StrUpdate);
            pstm.setString(1, p1.getPo_description());
            pstm.setString(2, p1.getPo_name());
            pstm.setString(3, p1.getPo_id());
            pstm.execute();
        } catch (Exception e) {
            System.out.println("updatePos " + e.getMessage());
        }
    }

    public void deletePoById(String idPo) {
        try {
            String strSelect
                    = "delete from flm_db.Po\n"
                    + "where flm_db.Po.Po_id = ?";
            PreparedStatement pstm = connection.prepareStatement(strSelect);
            pstm.setString(1, idPo);
            pstm.execute();

        } catch (SQLException e) {
            System.out.println("deletePoById: " + e.getMessage());
        }
    }

    public void addPoByCcode(String Po_id, String Po_name, String Po_description, String Ccode) {
        try {
            String strAdd = "INSERT INTO flm_db.`Po` (Po_id, Po_name, Po_description, curriculum_id)\n"
                    + "SELECT ?, ?, ?, c.curriculum_id\n"
                    + "FROM flm_db.curriculum c\n"
                    + "WHERE c.code = ?;";
            Po p = new Po();
            PreparedStatement pstm = connection.prepareStatement(strAdd);
            pstm.setString(1, Po_id);
            pstm.setString(2, Po_name);
            pstm.setString(3, Po_description);
            pstm.setString(4, Ccode);
            pstm.executeUpdate();
        } catch (Exception e) {
            System.out.println("addPoByCcode: " + e.getMessage());
        }
    }

    public boolean checkIdPo(String PoID) {
        try {
            String strSelect = "select flm_db.Po.Po_id from flm_db.Po \n"
                + "where flm_db.Po.Po_id =?";
        PreparedStatement pstm = connection.prepareStatement(strSelect);
        pstm.setString(1, PoID);
         ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                return true;
            }
        } catch (Exception e) {
            System.out.println("checkIdPo: " + e.getMessage());
        }
        return false;
    }

//    public static void main(String[] args) {
//        PoDAO pld = new PoDAO();
//        pld.addPoByCcode("1111", "PO2", "1234", "BIT_SE_K17B");
//    }
}
