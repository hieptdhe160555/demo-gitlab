/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Subject;

/**
 *
 * @author Admin
 */
public class SubjectDAO extends BaseDAO {

    public ArrayList<Subject> getListOfSubject(String search) {
        ArrayList<Subject> list = new ArrayList<>();
        try {
            String strSelect
                    = "select s.subject_id, s.subject_code, s.subject_name, st.setting_name ,s.subject_is_active, s.subject_description, s.subject_parent_id, p.subject_code as parent, s.subject_group_id  from flm_db.subject s\n"
                    + "left join flm_db.subject p on s.subject_parent_id = p.subject_id  \n"
                    + "join flm_db.setting st on st.setting_id = s.subject_type_id \n"
                    + "where s.subject_code like ? or \n"
                    + "s.subject_name like ?;";
            PreparedStatement pstm = connection.prepareStatement(strSelect);
            pstm.setString(1, "%" + search + "%");
            pstm.setString(2, "%" + search + "%");
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                Subject s = new Subject();
                s.setId(rs.getString(1));
                s.setCode(rs.getString(2));
                s.setName(rs.getString(3));
                s.setType(rs.getString(4));
                s.setIsActive((rs.getString(5).equals("1")) ? "true" : "false");
                s.setDescription(rs.getString(6));
                s.setParentId(rs.getString(7));
                s.setParentCode(rs.getString(8));
                s.setGroupId(rs.getString(9));
                list.add(s);
            }
            return list;
        } catch (SQLException e) {
            System.out.println("getListOfSubject: " + e.getMessage());
        }
        return null;
    }

    public void updateSubjectById(String id, String name, String code,String type, String active, String parentSubjectId, String description) {
        try {
            String strSelect = "update flm_db.subject\n"
                    + "set subject.subject_code = ?,\n"
                    + "subject.subject_name = ?,\n"
                    + "subject.subject_is_active = b?,\n"
                    + "subject.subject_parent_id = ?,\n"
                    + "subject.subject_type_id = ?,\n"
                    + "subject.subject_description =?\n"
                    + "where subject.subject_id = ?;";
            PreparedStatement pstm = connection.prepareStatement(strSelect);
            pstm.setString(1, code);
            pstm.setString(2, name);
            pstm.setString(3, active);
            if (parentSubjectId.isEmpty()) {
                pstm.setNull(4, java.sql.Types.VARCHAR);
            } else {
                pstm.setString(4, parentSubjectId);
            }
            pstm.setString(5, type);
            pstm.setString(6, description);
            pstm.setString(7, id);
            System.out.println(id);
            System.out.println(name);
            System.out.println(code);
            System.out.println(active);
            System.out.println(parentSubjectId);

            pstm.execute();
        } catch (SQLException e) {
            System.out.println("updateSubjectById: " + e.getMessage());
        }
    }

//    public void deleteSubjectById(String id) {
//        try {
//            String strSelect
//                    = "delete from flm_db.user_role \n"
//                    + "where user_id = ?;";
//            PreparedStatement pstm = connection.prepareStatement(strSelect);
//            pstm.setString(1, id);
//            pstm.execute();
//            strSelect = "delete from flm_db.user\n"
//                    + "where user_id = ?;";
//            pstm = connection.prepareStatement(strSelect);
//            pstm.setString(1, id);
//            pstm.execute();
//        } catch (SQLException e) {
//            System.out.println("deleteUserByUserId: " + e.getMessage());
//        }
//    }
    public void addSubject(String id, String name, String code, String parentid, String isActive, String description) {
        try {
            String strIns = "INSERT INTO flm_db.`subject` "
                    + "(subject_id, subject_code, subject_name, subject_is_active, subject_description, subject_parent_id) \n"
                    + "VALUES (?, ?, ?, b?, ?, ?);";
            PreparedStatement pstm = connection.prepareStatement(strIns);
            pstm.setString(1, id);
            pstm.setString(2, code);
            pstm.setString(3, name);
            pstm.setString(4, isActive);
            pstm.setString(5, description);
            pstm.setString(6, parentid);
            pstm.executeUpdate();
        } catch (SQLException e) {
            System.out.println("loi addSubject: " + e.getMessage());
        }
    }

    public void addSubjectNoneParent(String id, String name, String code, String isActive, String description) {
        try {
            String strIns = "INSERT INTO flm_db.`subject` "
                    + "(subject_id, subject_code, subject_name, subject_is_active, subject_description) \n"
                    + "VALUES (?, ?, ?, b?, ?);";
            PreparedStatement pstm = connection.prepareStatement(strIns);
            pstm.setString(1, id);
            pstm.setString(2, code);
            pstm.setString(3, name);
            pstm.setString(4, isActive);
            pstm.setString(5, description);
            pstm.executeUpdate();
        } catch (SQLException e) {
            System.out.println("loi addSubjectNoneParent: " + e.getMessage());
        }
    }

    public Subject checkDuplicateIdSubject(String id) {
        try {
            String strSelect = "SELECT * FROM flm_db.subject where subject_id=?;";
            PreparedStatement pstm = connection.prepareStatement(strSelect);
            pstm.setString(1, id);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                return new Subject(rs.getString(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getString(6));
            }
        } catch (SQLException e) {
            System.out.println("loi checkDuplicateIdSubject: " + e.getMessage());
        }
        return null;
    }

    public Subject getSubjectIdMax() {
        try {
            String strSelect = "SELECT MAX(subject_id) from flm_db.subject;";
            PreparedStatement pstm = connection.prepareStatement(strSelect);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                return new Subject(rs.getString(1));
            }
        } catch (SQLException e) {
            System.out.println("loi getSubjectIdMax: " + e.getMessage());
        }
        return null;
    }

    public Subject getParentCodeByParentId(String subject_id) {
        try {
            String strSelect = "SELECT * from flm_db.subject WHERE subject_id =?;";
            PreparedStatement pstm = connection.prepareStatement(strSelect);
            pstm.setString(1, subject_id);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                return new Subject(rs.getString(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getString(6));
            }
        } catch (SQLException e) {
            System.out.println("error getParentCodeByParentId: " + e.getMessage());
        }
        return null;
    }

    public Subject checkDuplicateCodeSubject(String code) {
        try {
            String strSelect = "SELECT * FROM flm_db.subject where subject_code=?;";
            PreparedStatement pstm = connection.prepareStatement(strSelect);
            pstm.setString(1, code);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                return new Subject(rs.getString(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getString(6));
            }
        } catch (SQLException e) {
            System.out.println("loi checkDuplicateCodeSubject: " + e.getMessage());
        }
        return null;
    }

    public Subject checkDuplicateNameSubject(String name) {
        try {
            String strSelect = "SELECT * FROM flm_db.subject where subject_name=?;";
            PreparedStatement pstm = connection.prepareStatement(strSelect);
            pstm.setString(1, name);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                return new Subject(rs.getString(1), rs.getString(2), rs.getString(3),
                        rs.getString(4), rs.getString(5), rs.getString(6));
            }
        } catch (SQLException e) {
            System.out.println("error checkDuplicateNameSubject: " + e.getMessage());
        }
        return null;
    }

    public ArrayList<Subject> getListCodeIdSubject() {
        ArrayList<Subject> listCodeIdSubject = new ArrayList<>();
        try {
            String strSelect = "SELECT subject_id, subject_code FROM flm_db.subject;";
            PreparedStatement pstm = connection.prepareStatement(strSelect);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                listCodeIdSubject.add(new Subject(rs.getString(1), rs.getString(2)));
            }
            return listCodeIdSubject;
        } catch (SQLException e) {
            System.out.println("error getListCodeIdSubject " + e.getMessage());
        }
        return null;
    }

 
    public static void main(String[] args) {
        SubjectDAO sd = new SubjectDAO();
//        ArrayList<Subject> listParentIDSubject = sd.getListCodeIdSubject();
//        for (Subject subject : listParentIDSubject) {
//            System.out.println(subject.getId());
//        }
//        ArrayList<CurriculumSubject> listComboDetail = sd.getSubjectComboByAdmin("268");
//        for (CurriculumSubject curriculumSubject : listComboDetail) {
//            System.out.println(curriculumSubject.getSubjectName());
//        }
//        System.out.println(sd.getParentCodeByParentId("3"));
    }
}
