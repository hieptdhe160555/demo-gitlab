package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
/**
 *
 * @author Admin
 */
public class BaseDAO {

    protected Connection connection;

    public BaseDAO() {
        String URL;
        String USER;
        String PASSWORD;
        try {
            URL = "jdbc:mysql://localhost:3306/FLM_DB";
            USER = "root";
            PASSWORD = "12345678";
            // driver register
            DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
            connection = DriverManager.getConnection(URL, USER, PASSWORD);
        } catch (SQLException ex) {
            Logger.getLogger(BaseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) {
        BaseDAO db = new BaseDAO();
        if (db.connection != null) {
            System.out.println("Thanh cong");
        } else {
            System.out.println("That Bai");
        }
    }
}
