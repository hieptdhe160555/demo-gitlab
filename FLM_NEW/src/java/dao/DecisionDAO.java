/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.Curriculum;
import model.Decision;

/**
 *
 * @author hp
 */
public class DecisionDAO extends BaseDAO{
    
    
    public Decision CheckDecisionByID(int decision_id) {
        try {
            String sql = "select * from decision where decision_id = ? ;  ";
            PreparedStatement pstm = connection.prepareStatement(sql);
            pstm.setInt(1, decision_id);
            ResultSet rs = pstm.executeQuery();
            if (rs.next()) {
                Decision d = new Decision(rs.getString(1), rs.getString(2), rs.getDate(3), rs.getString(4));
                System.out.println("run");
                return d;

            }

        } catch (SQLException e) {
            System.out.println("error CheckDecisionByID" + e.getMessage());
        }
        return null;
    }
    
}
