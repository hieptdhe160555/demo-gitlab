/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Plo;
import model.User;

/**
 *
 * @author Admin
 */
public class PloDAO extends BaseDAO {

    public ArrayList<Plo> getListPloByCode(String Code) {
        ArrayList<Plo> list = new ArrayList<>();
        try {
            String strSelect
                    = "select p.plo_id,p.plo_name,p.plo_description,p.is_active from plo as p join flm_db.curriculum as c\n"
                    + "on p.curriculum_id = c.curriculum_id\n"
                    + "where c.code = ?";
            PreparedStatement pstm = connection.prepareStatement(strSelect);
            pstm.setString(1, Code);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                String plo_id = rs.getString(1);
                String plo_name = rs.getString(2);
                String plo_description = rs.getString(3);
                String is_active = rs.getString(4);
                list.add(new Plo(plo_id, plo_name, plo_description,is_active));
            }
            return list;
        } catch (SQLException e) {
            System.out.println("getListPloByCode: " + e.getMessage());
        }
        return null;
    }

    public Plo getPLObyPloID(String plo_id) {
        try {
            String strSelect = "select p.plo_id,p.plo_name,p.plo_description,c.code,c.name, p.is_active from plo as p join curriculum as c\n"
                    + "on p.curriculum_id = c.curriculum_id\n"
                    + "where p.plo_id = ? ";
            PreparedStatement pstm = connection.prepareStatement(strSelect);
            pstm.setString(1, plo_id);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                return new Plo(
                        rs.getString(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getString(5),
                        rs.getString(6));

            }
        } catch (Exception e) {
            System.out.println("getPLObyNamePloAndCodeCurriculum " + e.getMessage());
        }
        return null;
    }

    public void updatePLOs(Plo p) {
        try {
            String StrUpdate = "update flm_db.plo \n"
                    + "	SET flm_db.plo.plo_description =?, flm_db.plo.plo_name =?, flm_db.plo.is_active = b?\n"
                    + "	where flm_db.plo.plo_id=?";
            PreparedStatement pstm = connection.prepareStatement(StrUpdate);
            pstm.setString(1, p.getPlo_description());
            pstm.setString(2, p.getPlo_name());
            pstm.setString(3, p.getIs_Active());
            pstm.setString(4, p.getPlo_id());
            pstm.execute();
        } catch (Exception e) {
            System.out.println("updatePLOs " + e.getMessage());
        }
    }

    public void deletePloById(String idPlo) {
        try {
            String strSelect
                    = "delete from flm_db.plo\n"
                    + "where flm_db.plo.plo_id = ?";
            PreparedStatement pstm = connection.prepareStatement(strSelect);
            pstm.setString(1, idPlo);
            pstm.execute();

        } catch (SQLException e) {
            System.out.println("deletePloById: " + e.getMessage());
        }
    }

    public void addPloByCcode(String plo_id, String plo_name, String plo_description, String Ccode, String is_Active) {
        try {
            String strAdd = "INSERT INTO flm_db.`plo` (plo_id, plo_name, plo_description, curriculum_id, is_active)\n"
                    + "SELECT ?, ?, ?, c.curriculum_id,b'1'\n"
                    + "FROM flm_db.curriculum c\n"
                    + "WHERE c.code = ?;";
            Plo p = new Plo();
            PreparedStatement pstm = connection.prepareStatement(strAdd);
            pstm.setString(1, plo_id);
            pstm.setString(2, plo_name);
            pstm.setString(3, plo_description);
            pstm.setString(4, Ccode);
           
            pstm.executeUpdate();
        } catch (Exception e) {
            System.out.println("addPloByCcode: " + e.getMessage());
        }
    }

    public boolean checkIdPlo(String ploID) {
        try {
            String strSelect = "select flm_db.plo.plo_id from flm_db.plo \n"
                + "where flm_db.plo.plo_id =?";
        PreparedStatement pstm = connection.prepareStatement(strSelect);
        pstm.setString(1, ploID);
         ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                return true;
            }
        } catch (Exception e) {
            System.out.println("checkIdPlo: " + e.getMessage());
        }
        return false;
    }

    public static void main(String[] args) {
        PloDAO pld = new PloDAO();
//        Plo p = new Plo("50", "PLO1", "12132133Have basic knowledge of social science, law and politics, national security and defense, contribute to form a worldview and scientific methodology....", b'1');
//        pld.updatePLOs(p);
        
        
    }
}
