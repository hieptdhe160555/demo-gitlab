/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.Curriculum;
import model.Group;

/**
 *
 * @author user
 */
public class GroupDAO extends BaseDAO {

    public ArrayList<Group> getListComboForUser(String id) {
        ArrayList<Group> listComboByCurriculumId = new ArrayList<>();
        try {
            String strSelect = "SELECT `group`.group_id, `group`.group_name, group_curriculum.curriculum_id, "
                    + "`group`.combo_subject FROM flm_db.group \n"
                    + "join flm_db.group_curriculum ON `group`.group_id = group_curriculum.group_id \n"
                    + "WHERE group_curriculum.curriculum_id = ? and `group`.combo_subject='1'; ";
            PreparedStatement pstm = connection.prepareStatement(strSelect);
            pstm.setString(1, id);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                listComboByCurriculumId.add(new Group(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4)));
            }
            return listComboByCurriculumId;
        } catch (SQLException e) {
            System.out.println("error getListComboByCurriculumId " + e.getMessage());
        }
        return null;
    }

    public ArrayList<Group> getListComboForAdmin(String id) {
        ArrayList<Group> listComboByCurriculumId = new ArrayList<>();
        try {
            String strSelect = "SELECT `group`.group_id, `group`.group_name, group_curriculum.curriculum_id, "
                    + "`group`.combo_subject FROM flm_db.group \n"
                    + "join flm_db.group_curriculum ON `group`.group_id = group_curriculum.group_id \n"
                    + "WHERE group_curriculum.curriculum_id = ?; ";
            PreparedStatement pstm = connection.prepareStatement(strSelect);
            pstm.setString(1, id);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                listComboByCurriculumId.add(new Group(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4)));
            }
            return listComboByCurriculumId;
        } catch (SQLException e) {
            System.out.println("error getListComboByCurriculumId " + e.getMessage());
        }
        return null;
    }

    public Group getComboByComboId(String comboid) {
        try {
            String strSelect = "SELECT `group`.group_id, `group`.group_name, group_curriculum.curriculum_id, "
                    + "`group`.combo_subject FROM flm_db.group \n"
                    + "join flm_db.group_curriculum ON `group`.group_id = group_curriculum.group_id \n"
                    + "WHERE `group`.group_id = ? and `group`.combo_subject='1';";
            PreparedStatement pstm = connection.prepareStatement(strSelect);
            pstm.setString(1, comboid);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                return new Group(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4));
            }
        } catch (SQLException e) {
            System.out.println("loi getComboByComboId: " + e.getMessage());
        }
        return null;
    }

    public static void main(String[] args) {
        GroupDAO gd = new GroupDAO();
//        ArrayList<Group> listComboByCurriculumId = gd.getListComboByCurriculumId("1");
//        for (Group group : listComboByCurriculumId) {
//            System.out.println(group);
//        }
        System.out.println(gd.getComboByComboId("268"));
    }

    public Group checkDuplicateGroupId(String groupid) {
        try {
            String strSelect = "SELECT * FROM flm_db.group where group_id=?;";
            PreparedStatement pstm = connection.prepareStatement(strSelect);
            pstm.setString(1, groupid);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                return new Group(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4));
            }
        } catch (SQLException e) {
            System.out.println("error checkDuplicateGroupId: " + e.getMessage());
        }
        return null;
    }

    public Group checkDuplicateGroupName(String groupname) {
        try {
            String strSelect = "SELECT * FROM flm_db.group where group_name=?;";
            PreparedStatement pstm = connection.prepareStatement(strSelect);
            pstm.setString(1, groupname);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                return new Group(rs.getString(1), rs.getString(2), rs.getString(3));
            }
        } catch (SQLException e) {
            System.out.println("error checkDuplicateGroupId: " + e.getMessage());
        }
        return null;
    }

    public Group getComboIdMax() {
        try {
            String strSelect = "SELECT MAX(group_id) from flm_db.group;";
            PreparedStatement pstm = connection.prepareStatement(strSelect);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                return new Group(rs.getString(1));
            }
        } catch (SQLException e) {
            System.out.println("error getComboIdMax: " + e.getMessage());
        }
        return null;
    }

    public void addCombo(String id, String groupname, String active) {
        try {
            String strIns = "INSERT INTO `flm_db`.`group` (`group_id`, `group_name`, `combo_subject`)"
                    + " VALUES (?, ?, b?);";
            PreparedStatement pstm = connection.prepareStatement(strIns);
            pstm.setString(1, id);
            pstm.setString(2, groupname);
            pstm.setString(3, active);
            pstm.executeUpdate();
        } catch (SQLException e) {
            System.out.println("error addComboNoneCurriculum: " + e.getMessage());
        }
    }

    public void addComboToGroupCurriculum(String id, String curriculumidcombo) {
        try {
            String strIns = "INSERT INTO `flm_db`.`group_curriculum` (`group_id`, `curriculum_id`) "
                    + "VALUES (?, ?);";
            PreparedStatement pstm = connection.prepareStatement(strIns);
            pstm.setString(1, id);
            pstm.setString(2, curriculumidcombo);
            pstm.executeUpdate();
        } catch (SQLException e) {
            System.out.println("error addComboToCurriculum: " + e.getMessage());
        }
    }

    public Group getComboByComboIdAdmin(String comboid) {
        try {
            String strSelect = "SELECT `group`.group_id, `group`.group_name, group_curriculum.curriculum_id, "
                    + "`group`.combo_subject FROM flm_db.group \n"
                    + "join flm_db.group_curriculum ON `group`.group_id = group_curriculum.group_id \n"
                    + "WHERE `group`.group_id = ?";
            PreparedStatement pstm = connection.prepareStatement(strSelect);
            pstm.setString(1, comboid);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                return new Group(rs.getString(1), rs.getString(2), rs.getString(3), rs.getString(4));
            }
        } catch (SQLException e) {
            System.out.println("loi getComboByComboId: " + e.getMessage());
        }
        return null;
    }

    public void updateCombo(String groupname, String active, String gId, String curriculumIdNew) {
        try {
            String strUpdateCombo = ""
                    + "UPDATE `flm_db`.`group` "
                    + "SET `group_name` = ?, "
                    + "`combo_subject` = b? "
                    + "WHERE (`group_id` = ?);";
            PreparedStatement pstm = connection.prepareStatement(strUpdateCombo);
            pstm.setString(1, groupname);
            pstm.setString(2, active);
            pstm.setString(3, gId);
            pstm.executeUpdate();
            String strUpdateComboToCurriculum = ""
                    + "UPDATE `flm_db`.`group_curriculum` "
                    + "SET `curriculum_id` = ? "
                    + "WHERE (`group_id` = ?);";
            pstm = connection.prepareStatement(strUpdateComboToCurriculum);
            pstm.setString(1, curriculumIdNew);
            pstm.setString(2, gId);
            pstm.executeUpdate();
        } catch (SQLException e) {
            System.out.println("error updateCombo: " + e.getMessage());
        }
    }

}
