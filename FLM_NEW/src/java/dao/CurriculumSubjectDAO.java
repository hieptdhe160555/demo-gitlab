/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import model.CurriculumSubject;

/**
 *
 * @author user
 */
public class CurriculumSubjectDAO extends BaseDAO {

    public ArrayList<CurriculumSubject> getSubjectComboByAdmin(String comboid) {
        ArrayList<CurriculumSubject> listComboDetail = new ArrayList<>();
        try {
            String strSelect = "SELECT cs.semester, s.subject_id, s.subject_code, s.subject_name, s.subject_description\n"
                    + "FROM curriculum_subject as cs join `subject` as s ON \n"
                    + "cs.subject_id = s.subject_id\n"
                    + "WHERE subject_group_id = ?;";
            PreparedStatement pstm = connection.prepareStatement(strSelect);
            pstm.setString(1, comboid);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                listComboDetail.add(new CurriculumSubject(rs.getString(1), rs.getString(2),
                        rs.getString(3), rs.getString(4), rs.getString(5)));
            }
            return listComboDetail;
        } catch (SQLException e) {
            System.out.println("error getComboDetailByComboId: " + e.getMessage());
        }
        return null;
    }

    public ArrayList<CurriculumSubject> getSubjectComboByUser(String comboid) {
        ArrayList<CurriculumSubject> listComboDetail = new ArrayList<>();
        try {
            String strSelect = "SELECT cs.semester, s.subject_id, s.subject_code, s.subject_name, s.subject_description\n"
                    + "FROM curriculum_subject as cs join `subject` as s ON \n"
                    + "cs.subject_id = s.subject_id\n"
                    + "WHERE subject_group_id = ? and subject_is_active='1';";
            PreparedStatement pstm = connection.prepareStatement(strSelect);
            pstm.setString(1, comboid);
            ResultSet rs = pstm.executeQuery();
            while (rs.next()) {
                listComboDetail.add(new CurriculumSubject(rs.getString(1), rs.getString(2),
                        rs.getString(3), rs.getString(4), rs.getString(5)));
            }
            return listComboDetail;
        } catch (SQLException e) {
            System.out.println("error getComboDetailByUser: " + e.getMessage());
        }
        return null;
    }

    public static void main(String[] args) {
        CurriculumSubjectDAO csDAO = new CurriculumSubjectDAO();

        ArrayList<CurriculumSubject> listComboDetail = csDAO.getSubjectComboByAdmin("268");
        for (CurriculumSubject curriculumSubject : listComboDetail) {
            System.out.println("id" + curriculumSubject.getSubjectId());
            System.out.println("code" + curriculumSubject.getSubjectCode());
            System.out.println("name" + curriculumSubject.getSubjectName());
            
        }
//        System.out.println(sd.getParentCodeByParentId("3"));
    }
}
