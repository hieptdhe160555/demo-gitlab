/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */


function deleteCombo(id) {
    if (confirm("Are you sure to delete Combo have ID is " + id)) {
        window.location = "/FLM_NEW/view/common/combo?action=delete?id=" + id;
    }
}

function selectBtForCombo(x) {
    var select = document.querySelector('#' + x).innerText;
    // change text status  
    if (select.toLowerCase() === 'true') {
        document.querySelector('#' + x).innerHTML = 'FALSE';
        document.getElementById("a-1").value = false;
    } else {
        document.querySelector('#' + x).innerHTML = 'TRUE';
        document.getElementById("a-1").value = true;
    }
}