<%-- 
    Document   : po
    Created on : May 23, 2023, 2:18:24 AM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" >
        <title>FLM_USER MANAGEMENT</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="Premium Bootstrap 4 Landing Page Template" >
        <meta name="keywords" content="Appointment, Booking, System, Dashboard, Health" >
        <meta name="author" content="Shreethemes" >
        <meta name="email" content="support@shreethemes.in" >
        <meta name="website" content="../../../../../../index.html" >
        <meta name="Version" content="v1.2.0" >

        <!-- favicon -->
        <link rel="shortcut icon" href="../../assets/images/flm-dark.png">
        <!-- Bootstrap -->
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet" type="text/css" >
        <!-- simplebar -->
        <link href="../../assets/css/simplebar.css" rel="stylesheet" type="text/css" >
        <!-- Select2 -->
        <link href="../../assets/css/select2.min.css" rel="stylesheet" >
        <!-- Date picker -->
        <link rel="stylesheet" href="../../assets/css/flatpickr.min.css">
        <link href="../../assets/css/jquery.timepicker.min.css" rel="stylesheet" type="text/css" >
        <!-- Icons -->
        <link href="../../assets/css/materialdesignicons.min.css" rel="stylesheet" type="text/css" >
        <link href="../../assets/css/remixicon.css" rel="stylesheet" type="text/css" >
        <link href="https://unicons.iconscout.com/release/v3.0.6/css/line.css"  rel="stylesheet">
        <!-- Css -->
        <link href="../../assets/css/style.min.css" rel="stylesheet" type="text/css" id="theme-opt" >
        <!--account css-->
        <link rel="stylesheet" href="../../Style/Account.css">

    </head>
    <style>
        .mb-0 tr:nth-child(odd) {
            background-color: #ffffff;
        }
        .mb-0 tr:hover {
            background-color: #ddd;
            ;
        }
        #tr{
            background-color:#396cf0;
            color: #ffffff;
        }
    </style>
    <body>
        <!-- Loader -->
        <div id="preloader">
            <div id="status">
                <div class="spinner">
                    <div class="double-bounce1"></div>
                    <div class="double-bounce2"></div>
                </div>
            </div>
        </div>
        <!-- Loader -->

        <div class="page-wrapper doctris-theme toggled">
            <%@include file="../common/sidebar.jsp" %>
            <!-- sidebar-wrapper  -->

            <!-- Start Page Content -->
            <main class="page-content bg-light">
                <%@include file="../common/headerforsidebar.jsp" %>
                <!--Duy-->

                <div class="container-fluid">

                    <div class="layout-specing">
                        <div class="d-md-flex justify-content-between">
                            <h5 class="mb-0">PO Management</h5><br>

                            <nav aria-label="breadcrumb" class="d-inline-block mt-4 mt-sm-0">
                                <ul class="breadcrumb bg-transparent rounded mb-0 p-0">
                                    <li class="breadcrumb-item"><a href="/FLM_NEW/view/common/home">HOME</a></li>
                                    <li class="breadcrumb-item">Curriculum</li>
                                    <li class="breadcrumb-item">POs</li>
                                    <li class="breadcrumb-item active" aria-current="page"><a href="account">PO List</a></li>
                                </ul>
                            </nav>

                        </div>
                        <P></P>
                        <h6 class="mb-0">Curriculum ID :   ${c.getCode()}</h6><br>
                        <h6 class="mb-0">Curriculum Name : ${c.getName()}</h6>
                        <div class=" row">
                            <div class="   col-12 mt-4">
                                <div class="table-responsive shadow rounded">
                                    <table class="  table table-center bg-white mb-0">
                                        <thead>
                                            <tr id="tr">
                                                <th class="border-bottom p-3" style="min-width: 50px;"></th>
                                                <th class="border-bottom p-3" style="min-width: 50px;">ID POs</th>
                                                <th class="border-bottom p-3" style="min-width: 90px;">PO Name</th>
                                                <th class="border-bottom p-3" style="min-width: 150px;">PO Description</th>
                                                    <c:choose>
                                                        <c:when test="${sessionScope.acc.getRoleName() == 'ADMIN'}">
                                                        <th class="border-bottom p-3" style="min-width: 120px;"></th>
                                                        </c:when>
                                                    </c:choose>
                                            </tr>
                                        </thead>

                                        <c:forEach  var="o" items="${ListPO}" >
                                            <c:set var="t" value="${t+1}"/>
                                            <tbody class="ListPO">

                                            <td class="border-bottom p-3" style="min-width: 30px;">${t}</td>
                                            <td class="border-bottom p-3" style="min-width: 30px;">${o.getPo_id()}</td>
                                            <td class="border-bottom p-3" style="min-width: 70px;">${o.getPo_name()}</td>
                                            <td class="border-bottom p-3" style="height: 100px;width: 1000px">${o.getPo_description()}</td>
                                            <c:choose>
                                                <c:when test="${sessionScope.acc.getRoleName() == 'ADMIN'}">
                                                    <td class="border-bottom p-3" style="min-width: 70px;">
                                                        <a class="btn btn-icon btn-pills btn-soft-success"   href="/FLM_NEW/view/admin/podetail?po_id=${o.po_id}&amp;Ccode=${c.code}"><i class="uil uil-pen"></i></a>
                                                        <a href="#" class="btn btn-icon btn-pills btn-soft-danger" onclick="deletePO('${o.getPo_id()}')" > <i class="uil uil-trash" ></i></a>
                                                    </td>
                                                </c:when>
                                            </c:choose>
                                            </tbody>
                                        </c:forEach>
                                    </table>
                                    <form id="deletePO" action="PO?mod=2&Ccode=${c.getCode()}" method="post">
                                        <input id="delete" type="hidden" name="IdPO">
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!--end row-->
                    </div>
                </div><!--end container-->

                <!-- Footer Start -->
                <%@include file="../common/footerforsidebar.jsp" %>
                <!--end footer-->
                <!-- End -->
            </main>
            <!--End page-content" -->
        </div>
        <!-- page-wrapper -->
        <!-- javascript -->
        </script>
        <script src="../../assets/js/jquery.min.js"></script>
        <script src="../../assets/js/bootstrap.bundle.min.js"></script>
        <!-- simplebar -->
        <script src="../../assets/js/simplebar.min.js"></script>
        <!-- Select2 -->
        <script src="../../assets/js/select2.min.js"></script>
        <script src="../../assets/js/select2.init.js"></script>
        <!-- Datepicker -->
        <script src="../../assets/js/flatpickr.min.js"></script>
        <script src="../../assets/js/flatpickr.init.js"></script>
        <!-- Datepicker -->
        <script src="../../assets/js/jquery.timepicker.min.js"></script> 
        <script src="../../assets/js/timepicker.init.js"></script> 
        <!-- Icons -->
        <script src="../../assets/js/feather.min.js"></script>
        <!-- Main Js -->
        <script src="../../assets/js/app.js"></script>
        <script>
        function deletePO(x){
            if(confirm('Do you want to remove the user with Id:'+x)===true){
            document.getElementById('delete').value = x;
            document.getElementById('deletePO').submit();
            }
        }
         </script> 
    </body>

</html>
