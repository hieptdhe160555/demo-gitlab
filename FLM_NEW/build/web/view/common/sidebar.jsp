<%-- 
    Document   : sidebar
    Created on : May 26, 2023, 4:21:10 PM
    Author     : Admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>

    </head>
    <body>

        <!--<div class="page-wrapper doctris-theme toggled">-->
        <nav id="sidebar" class="sidebar-wrapper">
            <div class="sidebar-content" data-simplebar style="height: calc(100% - 60px);">
                <div class="sidebar-brand">
                    <a href="/FLM_NEW/view/common/home">
                        <!--<a href="index.html">-->
                        <img src="../../assets/images/flm-dark.png" height="150" class="logo-light-mode" alt="">
                        <img src="../../assets/images/flm-dark.png" height="150" class="logo-dark-mode" alt="">
                    </a>
                </div>

                <c:if test="${sessionScope.acc.getRoleName() == 'ADMIN'}" >
                    <ul class="sidebar-menu pt-3">

                        <li class="sidebar-dropdown">
                            <a href="javascript:void(0)"><i class="uil uil-setting me-2 d-inline-block"></i>Setting</a>
                            <div class="sidebar-submenu">
                                <ul>
                                    <li><a href="/FLM_NEW/view/admin/setting">Setting List</a></li>
                                    <li><a href="/FLM_NEW/view/admin/settingdetail?&mod=1">Add New Setting</a></li>
                                </ul>
                            </div>
                        </li>


                        <li class="sidebar-dropdown">
                            <a href="javascript:void(0)"><i class="uil uil-user me-2 d-inline-block"></i>Account</a>
                            <div class="sidebar-submenu">
                                <ul>
                                    <li><a href="/FLM_NEW/view/admin/account">Account List</a></li>
                                    <li><a href="/FLM_NEW/view/admin/adduser">Add New User</a></li>
                                </ul>
                            </div>
                        </li>
                        <li class="sidebar-dropdown">
                            <a href="/FLM_NEW/view/curriculum/listcurriculum" ><i class="uil uil-setting me-2 d-inline-block"></i>Curriculum</a>                
                            <c:if test="${op == 4}">
                                <div class="sidebar-submenu">
                                    <ul>
                                        <li><a href="/FLM_NEW/view/curriculum/overviewcurriculum?Ccode=${c.getCode()}">Overview</a></li>
                                        <li><a href="/FLM_NEW/view/common/po?Ccode=${c.getCode()}">POs</a></li>
                                        <li><a href="/FLM_NEW/view/admin/addPLOs?Ccode=${c.getCode()}">Add New PLO</a></li>
                                        <li><a href="/FLM_NEW/view/common/plo?Ccode=${c.getCode()}">PLOs</a></li> 
                                        <c:if test="${opplo == 0}">
                                        <li><a  href="/FLM_NEW/view/admin/plodetails?Ccode=${c.code}&amp;plo_id=${p.plo_id}">PLOs Details</a></li>
                                        </c:if>
                                        <li hidden=""><a  href="/FLM_NEW/view/admin/plodetails?Ccode=${c.code}"></a></li>
                                        <li><a href="/FLM_NEW/view/common/mappingPLO_PO?Ccode=${c.getCode()}">PLO-POs</a></li>
                                        <li><a href="/FLM_NEW/view/common/curriculumsubject?action=list&id=${c.curriculum_id}">Subjects</a></li>
                                        <li><a href="/FLM_NEW/view/common/combo?action=combolist&curriculumid=${c.curriculum_id}">Combo</a></li>

                                    </ul>

                                </div>
                            </c:if>
                        </li>
                        <li>
                            <a href="/FLM_NEW/view/common/subjectpredeccessors"><i class="uil uil-setting me-2 d-inline-block"></i>Subject Predecessor</a>     

                        </li>
                        <li>  
                            <a href="/FLM_NEW/view/common/subjectsuccessors"><i class="uil uil-setting me-2 d-inline-block"></i>Subject Successor</a>
                        </li>
                    </ul>

                    <ul class="sidebar-menu pt-3">
                        <li class="sidebar-dropdown">
                            <a href="javascript:void(0)"><i class="uil uil-user me-2 d-inline-block"></i>Subject Management</a>
                            <div class="sidebar-submenu">
                                <ul>
                                    <li><a href="/FLM_NEW/view/admin/subject?action=list">Subject List</a></li>
                                    <li><a href="/FLM_NEW/view/admin/subject?action=add">Add New Subject</a></li>
                                </ul>
                            </div>
                        </li><!-- comment -->

                        <li class="sidebar-dropdown">
                            <a href="javascript:void(0)"><i class="uil uil-user me-2 d-inline-block"></i>Training Curriculum</a>
                            <div class="sidebar-submenu">
                                <ul>
                                    <li><a href="/FLM_NEW/view/curriculum/listcurriculum">Curriculum List</a></li>
                                    <li><a href="/FLM_NEW/view/curriculum/newcurriculum">New Curriculum</a></li>
                                </ul>
                            </div>
                        </li><!-- comment -->
                    </ul>




                </c:if>

                <c:if test="${sessionScope.acc.getRoleName() == null}">
                    <ul class="sidebar-menu pt-3">
                        <li class="sidebar-dropdown">
                            <a href="/FLM_NEW/view/curriculum/listcurriculum" ><i class="uil uil-setting me-2 d-inline-block"></i>Curriculum</a>                
                            <c:if test="${op == 4}">
                                <div class="sidebar-submenu">
                                    <ul>
                                        <li><a href="/FLM_NEW/view/curriculum/overviewcurriculum?Ccode=${c.getCode()}">Overview</a></li>
                                        <li><a href="/FLM_NEW/view/common/po?Ccode=${c.getCode()}">POs</a></li>
                                        <li><a href="/FLM_NEW/view/common/plo?Ccode=${c.getCode()}">PLOs</a></li>

                                        
                                        <li><a href="/FLM_NEW/view/curriculum/combolist?Ccode=${c.curriculum_id}">Combo</a></li>

                                        <li><a href="/FLM_NEW/view/common/mappingPLO_PO?Ccode=${c.getCode()}">PLO-POs</a></li>
                                        

                                    </ul>

                                </div>
                            </c:if>
                        </li>
                        <li>
                            <a href="subjectpredecessors"><i class="uil uil-setting me-2 d-inline-block"></i>Subject Predecessor</a>     

                        </li>
                        <li>  
                            <a href="subjectsuccessors"><i class="uil uil-setting me-2 d-inline-block"></i>Subject Successor</a>
                        </li>
                    </ul>
                </c:if>

                <c:if test="${sessionScope.acc.getRoleName() == 'STUDENT'}" >

                    <ul class="sidebar-menu pt-3">
                        <li class="sidebar-dropdown" >
                            <a href="/FLM_NEW/view/curriculum/listcurriculum"><i class="uil uil-setting me-2 d-inline-block"></i>Curriculum</a>    
                            <c:if test="${op == 4}">
                                <div class="sidebar-submenu">
                                    <ul>
                                        <li><a href="/FLM_NEW/view/curriculum/overviewcurriculum?Ccode=${c.getCode()}">Overview</a></li>
                                        <li><a href="/FLM_NEW/view/common/po?Ccode=${c.getCode()}">POs</a></li>
                                        <li><a href="/FLM_NEW/view/common/plo?Ccode=${c.getCode()}">PLOs</a></li>
                                        <li><a href="#">PLO-POs</a></li>
                                        <li><a href="/FLM_NEW/view/curriculum/combolist?Ccode=${c.curriculum_id}">Combo</a></li>
                                    </ul>

                                </div>
                            </c:if>
                        </li>

                        <li>
                            <a href="#"><i class="uil uil-setting me-2 d-inline-block"></i>Syllabus</a>     
                        </li>

                        <li>
                            <a href="subjectpredecessors"><i class="uil uil-setting me-2 d-inline-block"></i>Subject Predecessor</a>     
                        </li>

                        <li>  
                            <a href="subjectsuccessors"><i class="uil uil-setting me-2 d-inline-block"></i>Subject Successor</a>
                        </li>
                    </ul>

                </c:if>
                <c:if test="${sessionScope.acc.getRoleName() == 'TEACHER'}" >

                    <ul class="sidebar-menu pt-3">
                        <li class="sidebar-dropdown" >
                            <a href="/FLM_NEW/view/curriculum/listcurriculum"><i class="uil uil-setting me-2 d-inline-block"></i>Curriculum</a>    
                            <c:if test="${op == 4}">
                                <div class="sidebar-submenu">
                                    <ul>
                                        <li><a href="/FLM_NEW/view/curriculum/overviewcurriculum?Ccode=${c.getCode()}">Overview</a></li>
                                        <li><a href="/FLM_NEW/view/common/po?Ccode=${c.getCode()}">POs</a></li>
                                        <li><a href="/FLM_NEW/view/common/plo?Ccode=${c.getCode()}">PLOs</a></li>
                                        <li><a href="#">PLO-POs</a></li>
                                        <li><a href="/FLM_NEW/view/curriculum/combolist?Ccode=${c.curriculum_id}">Combo</a></li>
                                    </ul>

                                </div>
                            </c:if>
                        </li>

                        <li>
                            <a href="#"><i class="uil uil-setting me-2 d-inline-block"></i>Syllabus</a>     
                        </li>

                        <li>
                            <a href="subjectpredecessors"><i class="uil uil-setting me-2 d-inline-block"></i>Subject Predecessor</a>     
                        </li>
                        <li>  
                            <a href="subjectsuccessors"><i class="uil uil-setting me-2 d-inline-block"></i>Subject Successor</a>
                        </li>
                    </ul>

                </c:if>
                <c:if test="${sessionScope.acc.getRoleName() == 'REVIEWER'}" >

                    <ul class="sidebar-menu pt-3">
                        <li class="sidebar-dropdown" >
                            <a href="/FLM_NEW/view/curriculum/listcurriculum"><i class="uil uil-setting me-2 d-inline-block"></i>Curriculum</a>    
                            <c:if test="${op == 4}">
                                <div class="sidebar-submenu">
                                    <ul>
                                        <li><a href="/FLM_NEW/view/curriculum/overviewcurriculum?Ccode=${c.getCode()}">Overview</a></li>
                                        <li><a href="/FLM_NEW/view/common/po?Ccode=${c.getCode()}">POs</a></li>
                                        <li><a href="/FLM_NEW/view/common/plo?Ccode=${c.getCode()}">PLOs</a></li>
                                        <li><a href="#">PLO-POs</a></li>
                                        <li><a href="/FLM_NEW/view/curriculum/combolist?Ccode=${c.curriculum_id}">Combo</a></li>
                                    </ul>

                                </div>
                            </c:if>
                        </li>

                        <li>
                            <a href="subjectpredecessors"><i class="uil uil-setting me-2 d-inline-block"></i>Subject Predecessor</a>     
                        </li>
                        <li>  
                            <a href="subjectsuccessors"><i class="uil uil-setting me-2 d-inline-block"></i>Subject Successor</a>
                        </li>

                        <li class="sidebar-dropdown">
                            <a href="javascript:void(0)"><i class="uil uil-user me-2 d-inline-block"></i>Dashboard</a>
                            <div class="sidebar-submenu">
                                <ul>
                                    <li><a href="/FLM_NEW/view/admin/account">Review syllabi</a></li>

                                </ul>
                            </div>
                        </li>
                    </ul>

                </c:if>

                <c:if test="${sessionScope.acc.getRoleName() == 'DESIGNER'}" >

                    <ul class="sidebar-menu pt-3">
                        <li class="sidebar-dropdown" >
                            <a href="/FLM_NEW/view/curriculum/listcurriculum"><i class="uil uil-setting me-2 d-inline-block"></i>Curriculum</a>    
                            <c:if test="${op == 4}">
                                <div class="sidebar-submenu">
                                    <ul>
                                        <li><a href="/FLM_NEW/view/curriculum/overviewcurriculum?Ccode=${c.getCode()}">Overview</a></li>
                                        <li><a href="/FLM_NEW/view/common/po?Ccode=${c.getCode()}">POs</a></li>
                                        <li><a href="/FLM_NEW/view/common/plo?Ccode=${c.getCode()}">PLOs</a></li>
                                        <li><a href="#">PLO-POs</a></li>
                                        <li><a href="/FLM_NEW/view/curriculum/combolist?Ccode=${c.curriculum_id}">Combo</a></li>
                                    </ul>

                                </div>
                            </c:if>
                        </li>

                        <li>
                            <a href="subjectpredecessors"><i class="uil uil-setting me-2 d-inline-block"></i>Subject Predecessor</a>     
                        </li>
                        <li>  
                            <a href="subjectsuccessors"><i class="uil uil-setting me-2 d-inline-block"></i>Subject Successor</a>
                        </li>

                        <li class="sidebar-dropdown">
                            <a href="javascript:void(0)"><i class="uil uil-user me-2 d-inline-block"></i>Dashboard</a>
                            <div class="sidebar-submenu">
                                <ul>
                                    <li><a href="/FLM_NEW/view/admin/account">Design syllabi</a></li>

                                </ul>
                            </div>
                        </li>
                    </ul>

                </c:if>

                <c:if test="${sessionScope.acc.getRoleName() == 'CRDD STAFF'}" >

                    <ul class="sidebar-menu pt-3">
                        <li class="sidebar-dropdown" >
                            <a href="/FLM_NEW/view/curriculum/listcurriculum"><i class="uil uil-setting me-2 d-inline-block"></i>Curriculum</a>    
                            <c:if test="${op == 4}">
                                <div class="sidebar-submenu">
                                    <ul>
                                        <li><a href="/FLM_NEW/view/curriculum/overviewcurriculum?Ccode=${c.getCode()}">Overview</a></li>
                                        <li><a href="/FLM_NEW/view/common/po?Ccode=${c.getCode()}">POs</a></li>
                                        <li><a href="/FLM_NEW/view/common/plo?Ccode=${c.getCode()}">PLOs</a></li>
                                        <li><a href="#">PLO-POs</a></li>
                                        <li><a href="/FLM_NEW/view/curriculum/combolist?Ccode=${c.curriculum_id}">Combo</a></li>
                                    </ul>

                                </div>
                            </c:if>
                        </li>

                        <li>
                            <a href="#"><i class="uil uil-setting me-2 d-inline-block"></i>Syllabus</a>     
                        </li>

                        <li>
                            <a href="subjectpredecessors"><i class="uil uil-setting me-2 d-inline-block"></i>Subject Predecessor</a>     
                        </li>
                        <li>  
                            <a href="subjectsuccessors"><i class="uil uil-setting me-2 d-inline-block"></i>Subject Successor</a>
                        </li>


                    </ul>
                    <ul class="sidebar-menu pt-3">
                        <li class="sidebar-dropdown">
                            <a href="javascript:void(0)"><i class="uil uil-user me-2 d-inline-block"></i>Training Curriculum</a>
                            <div class="sidebar-submenu">
                                <ul>
                                    <li><a href="/FLM_NEW/view/curriculum/listcurriculum">Curriculum List</a></li>
                                    <li><a href="/FLM_NEW/view/curriculum/newcurriculum">New Curriculum</a></li>                                  
                                </ul>
                            </div>
                        </li>
                    </ul>

                </c:if>
                <c:if test="${sessionScope.acc.getRoleName() == 'CRDD HEAD'}" >

                    <ul class="sidebar-menu pt-3">
                        <li class="sidebar-dropdown" >
                            <a href="/FLM_NEW/view/curriculum/listcurriculum"><i class="uil uil-setting me-2 d-inline-block"></i>Curriculum</a>    
                            <c:if test="${op == 4}">
                                <div class="sidebar-submenu">
                                    <ul>
                                        <li><a href="/FLM_NEW/view/curriculum/overviewcurriculum?Ccode=${c.getCode()}">Overview</a></li>
                                        <li><a href="/FLM_NEW/view/common/po?Ccode=${c.getCode()}">POs</a></li>
                                        <li><a href="/FLM_NEW/view/common/plo?Ccode=${c.getCode()}">PLOs</a></li>
                                        <li><a href="#">PLO-POs</a></li>
                                        <li><a href="/FLM_NEW/view/curriculum/combolist?Ccode=${c.curriculum_id}">Combo</a></li>
                                    </ul>

                                </div>
                            </c:if>
                        </li>
                        <li>
                            <a href="#"><i class="uil uil-setting me-2 d-inline-block"></i>Syllabus</a>     
                        </li>

                        <li>
                            <a href="subjectpredecessors"><i class="uil uil-setting me-2 d-inline-block"></i>Subject Predecessor</a>     
                        </li>
                        <li>  
                            <a href="subjectsuccessors"><i class="uil uil-setting me-2 d-inline-block"></i>Subject Successor</a>
                        </li>


                    </ul>
                    <ul class="sidebar-menu pt-3">
                        <li class="sidebar-dropdown">
                            <a href="javascript:void(0)"><i class="uil uil-user me-2 d-inline-block"></i>Training Curriculum</a>
                            <div class="sidebar-submenu">
                                <ul>
                                    <li><a href="/FLM_NEW/view/curriculum/listcurriculum">Curriculum List</a></li>
                                    <li><a href="/FLM_NEW/view/curriculum/newcurriculum">New Curriculum</a></li>

                                </ul>
                            </div>
                        </li>
                    </ul>

                </c:if>



            </div>
        </nav>

    </body>
</html>
